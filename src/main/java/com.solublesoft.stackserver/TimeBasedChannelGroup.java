package com.solublesoft.stackserver;

import io.netty.channel.Channel;
import io.netty.channel.ChannelId;
import io.netty.channel.group.ChannelGroupFuture;
import io.netty.channel.group.ChannelMatcher;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.EventExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

/**
 * Created by oseik on 12/11/16.
 * <p>
 * The defaultChannelGroup class will keep track of active connections but cannot tell which ones are old. This
 * extension is backed by a thread-safe FIFO which has a tuple of the channel ID and the time the connection came in
 */

public class TimeBasedChannelGroup extends DefaultChannelGroup {
    Logger logger = LoggerFactory.getLogger(TimeBasedChannelGroup.class);

    public final ConcurrentLinkedQueue<AbstractMap.SimpleEntry<ChannelId, Long>> connectionTimes = new ConcurrentLinkedQueue<>();

    public TimeBasedChannelGroup(EventExecutor executor) {
        super(executor, false);
    }

    /**
     * A convenience method that will remove a queued channelId
     *
     * @param channelId
     */
    private void iterateToRemove(ChannelId channelId) {
        Iterator<AbstractMap.SimpleEntry<ChannelId, Long>> it = connectionTimes.iterator();
        while (it.hasNext()) {
            Map.Entry<ChannelId, Long> currentItem = it.next();
            if (currentItem.getKey() == channelId) {
                it.remove();
            }
        }
    }

    /**
     * Necessary for when channels are closed ... we need to clean up our own list so we iterate and remove it
     *
     * @param ch
     * @return
     */
    @Override
    public boolean remove(Object ch) {
        boolean retVal = super.remove(ch);
        if (retVal)
            if (ch instanceof Channel)
                iterateToRemove(((Channel) ch).id());

        return retVal;
    }


    /**
     * Overriding the parent method so that we can keep track of adding the channel's addition time
     *
     * @param channel
     * @return
     */
    @Override
    public boolean add(Channel channel) {
        boolean retVal = super.add(channel);
        if (retVal)
            connectionTimes.add(new AbstractMap.SimpleEntry<>(channel.id(), System.currentTimeMillis()));
        return retVal;
    }


    /**
     * Method to close a matched channel and also to remove it from the queue
     *
     * @param matcher
     * @param channelId
     * @return
     */
    public ChannelGroupFuture close(ChannelMatcher matcher, ChannelId channelId) {
        ChannelGroupFuture retVal = super.close(matcher);
        if (retVal.isSuccess())
            iterateToRemove(channelId);

        return retVal;
    }

    /**
     * Method to return the channel ID of a connection that can be popped
     *
     * @return
     */
    public ChannelId getExpiredConnection() {
        ChannelId toPop  = null;
        Map.Entry<ChannelId, Long> queueHead = connectionTimes.peek();
        if (queueHead != null)
            if (System.currentTimeMillis() - queueHead.getValue() > 10000)
                toPop =  queueHead.getKey();
        if (toPop != null){
            logger.debug("Found a stale connection ... "+toPop);
        }
        return toPop;
    }

    public Map<String, Integer> getSizes() {
        Map<String, Integer> currChannels = new HashMap<>();
        currChannels.put("timed", connectionTimes.size());
        currChannels.put("netty", super.size());
        return currChannels;
    }

    public String debugChannels() {
        return connectionTimes.stream()
                .map(entry -> "{"+entry.getKey() + " : " + entry.getValue().toString()+"}")
                .collect(Collectors.joining(" , "));
    }

    public String getSizesAsString() {
        return getSizes().entrySet().stream()
                .map(entry -> entry.getKey() + " : " + entry.getValue().toString())
                .collect(Collectors.joining(" "));
    }
}
