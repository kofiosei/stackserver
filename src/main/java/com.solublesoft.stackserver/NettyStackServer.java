package com.solublesoft.stackserver;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.bytes.ByteArrayEncoder;

/**
 * Created by oseik on 12/10/16.
 */
public class NettyStackServer {
    public static final int NUMCONCURRENT = 100;

    private static final int serverPort = 8080;
    private static final int debugPort = 8081;

    public static void main(String... args) throws InterruptedException {
        //the thread group that handles accepts and then the delegation group
        EventLoopGroup bossGrp = new NioEventLoopGroup();
        EventLoopGroup workerGrp = new NioEventLoopGroup();

        EventLoopGroup bossGrpDebug = new NioEventLoopGroup();
        EventLoopGroup workerGrpDebug = new NioEventLoopGroup();

        ChannelFuture cfServer = null;
        ChannelFuture cfDebug = null;

        try {
            ServerBootstrap sb = new ServerBootstrap();
            ServerBootstrap sbd = new ServerBootstrap();


            sb.group(bossGrp, workerGrp)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ch.pipeline()
                                    //the handler to accumulate all the data first
                                    .addLast(new PushPopDecoder())
                                    // the handler that converts byte arrays to bytebufs for netty
                                    .addLast(new ByteArrayEncoder())
                                    //the actual handler that does the heavy lifting
                                    .addLast( new PushPopHandler());
                        }
                    });

             cfServer = sb.bind(serverPort);

            sbd.group(bossGrpDebug, workerGrpDebug)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {

                            ch.pipeline()
                                    //for output
                                    .addLast(new ByteArrayEncoder())
                                    // the debugging handler
                                    .addLast(new DebugHandler());
                        }
                    });
             cfDebug = sbd.bind(debugPort);

        } finally {
            cfServer.sync().channel().closeFuture().sync();
            cfDebug.sync().channel().closeFuture().sync();
            //shutdown all the threads
            bossGrp.shutdownGracefully();
            workerGrp.shutdownGracefully();

            bossGrpDebug.shutdownGracefully();
            workerGrpDebug.shutdownGracefully();
        }
    }
}
