package com.solublesoft.stackserver;

import java.io.Serializable;
import java.util.Iterator;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by oseik on 12/10/16.
 */
public class PhiloLifo implements Serializable {

    //A blocking queue is used because it's thread safe and blocks
    //TODO ... change the implementation to something else that will not hangup the thread because using this object
    //locks up the thread. Forcing us to use a timeout hack to then get the channel close event firing.

    private static BlockingDeque<LifoEntry> internalStack = new LinkedBlockingDeque<>(NettyStackServer.NUMCONCURRENT);
    public static PhiloLifo SINGLETON = new PhiloLifo();

    private Object getSingleton() {
        return SINGLETON;
    }

    /**
     * A blocking method that will remove the latest added item of the stack.
     *
     * @return LifoEntry
     * @throws InterruptedException Throw an exception so that the application can deal with application logic
     */
    public LifoEntry pop() throws InterruptedException {
        //Uhh ... this blocks indefinitely so it can be tricky
        return PhiloLifo.internalStack.takeFirst();
    }

    /**
     *
     * A blocking method that adds an item to the stack
     *
     * @param le LifoEntry item to add to the stack
     * @return
     * @throws InterruptedException Throw an exception so that the application can deal with application logic
     */
    public void push(LifoEntry le) throws InterruptedException {
        le.setTime();
        //blocks indefinitely
        PhiloLifo.internalStack.putFirst(le);
    }

    /**
     * Removes all items from the stack
     */
    public void resetStack(){
        PhiloLifo.internalStack.clear();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Iterator<LifoEntry> it = internalStack.iterator();
        while (it.hasNext())
            sb.append("\n").append(it.next().toString());

        sb.append("\n size: ").append(internalStack.size()).append("\n");
        return sb.toString();
    }
}
