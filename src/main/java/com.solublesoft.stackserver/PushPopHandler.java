package com.solublesoft.stackserver;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelMatchers;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by oseik on 12/10/16.
 */
//public class PushPopHandler extends SimpleChannelInboundHandler<ByteBuf> {
public class PushPopHandler extends SimpleChannelInboundHandler<StackOperationDirective> {

    Logger logger = LoggerFactory.getLogger(LifoEntry.class);
    public static final TimeBasedChannelGroup liveConns = new TimeBasedChannelGroup(GlobalEventExecutor.INSTANCE);
    private static final AtomicInteger numActiveConnPushes = new AtomicInteger();
    private static final AtomicInteger numActiveConnPops = new AtomicInteger();
    ExecutorService forBlockingOpsService = Executors.newFixedThreadPool(NettyStackServer.NUMCONCURRENT + 10);
    private Future future;
    private byte[] capturedData;
    private OperationType clientOp;

    public enum OperationType {
        PUSH, POP
    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx) throws Exception {
        PhiloLifo pl = new PhiloLifo();
        //OK ... this isn't awesome ... we are blocking on access to the liveConns
        //but if we don't do this ... it is possible the check from the 99th connection now going to the 100th
        // might end up being the 101 th because another connection may have added things just after the size check
        // but before the add would register an increase in the liveConns size count

        //synchronized slows things in java but only using it to increment/decrement could lead to multiple threads all
        //having wrong counts on the number of active connections
        synchronized (liveConns) {
            ChannelId poppable = checkAndCutOldConnections();
            if ((liveConns.size() < NettyStackServer.NUMCONCURRENT ) || (poppable != null)) {
                logger.debug("adding channel " + ctx.channel().id() + " to live conns : " + liveConns.debugChannels());
                liveConns.add(ctx.channel());
            } else {
                if (poppable == null)
                    logger.debug("QUITING: size is over " + liveConns.size());
                else
                    logger.debug("QUITING poppable is " + poppable);
                shortCircuit(ctx);
            }
        }
    }


    /**
     * Does the dirty work of finding old connections and disconnecting.
     *
     * @return a channelId or null if nothing got disconnected
     */
    private ChannelId checkAndCutOldConnections() {

        ChannelId chId = liveConns.getExpiredConnection();
        if (chId != null) {
            Channel myChannel = liveConns.find(chId);
            liveConns.close(ChannelMatchers.is(myChannel), myChannel.id());
            logger.debug("!!!! we are popping off a stale connection with id " + chId.toString());
        }
        return chId;
    }

    /**
     * sends the busy flag to the client
     * @param ctx
     */
    public void shortCircuit(ChannelHandlerContext ctx) {
        logger.debug("short circuiting right away " + ctx.channel().id().toString());
        byte[] out = new byte[1];
        out[0] = (byte)0xFF;
        ctx.writeAndFlush(out);
        ctx.close();
    }


    /**
     * The meat and potatoes of the logic
     * @param ctx
     * @param stackOp
     * @throws Exception
     */
    @Override
    public void channelRead0(ChannelHandlerContext ctx, StackOperationDirective stackOp) throws Exception {
        PhiloLifo pl = PhiloLifo.SINGLETON;
        clientOp = stackOp.getOperation();
        if (clientOp == OperationType.PUSH) {

            numActiveConnPushes.incrementAndGet();

            //apparently bytebuf don't like being accessed by a different thread. Earlier attempts all led to weird
            //reading issues which blocked
            byte[] inputData = stackOp.getData();

            //So ... we do this in another thread because we are using a java blockingdequeue ... which would lock up
            // netty's thread for dealing with this connection. this blocking behavior would prevent the channelInActive
            // event from firing for the connection because it'd be stuck waiting for the blockingqueue lock. So we
            // fire it off in another thread and free up the connection thread for netty to react to events . 

            future = forBlockingOpsService.submit(() -> {
                try {
                    LifoEntry le = new LifoEntry(inputData);
                    capturedData = le.getData();
                    pl.push(le);
                    ctx.writeAndFlush(new byte[1]);
                    ctx.close();
                    logger.debug("succesfully pushed " + new String(capturedData) + " channel : " + ctx.channel().id().toString());
                } catch (InterruptedException ie) {
                    logger.error("exception in push thread (channel id : " + ctx.channel().id() + ") with data:" +
                            new String(capturedData) + " >> " + ie.getLocalizedMessage());
                    Thread.currentThread().interrupt();
                    ctx.close();
                }
            });
        } else {
            PushPopHandler.numActiveConnPops.incrementAndGet();

            //same new thread behavior for pops.
            future = forBlockingOpsService.submit(() -> {
                try {
                    // get the data from the queue
                    LifoEntry le = pl.pop();
                    //if we actually got data ...
                    if (le != null) {
                        byte[] popped = le.getData();
                        //create a new byte array and make room for the header
                        byte[] toWrite = new byte[popped.length + 1];
                        //write the length of the data which should be no more than 2^7
                        toWrite[0] = (byte) popped.length;
                        System.arraycopy(popped, 0, toWrite, 1, popped.length); //;new byte[popped.length+1];
                        logger.debug("popped the data "+new String(popped)+ " of length "+new String(toWrite).length());
                        ctx.writeAndFlush(toWrite);
                        ctx.close();
                    }
                } catch (InterruptedException ie) {
                    logger.error("exception in pop thread >>>" + ie.getLocalizedMessage());
                    ctx.close();
                }
            });
        }
    }

    /**
     * This method should fire when the client closes the connection. So timeout events etc etc should make their way
     * here
     *
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        synchronized (liveConns) {
            super.channelInactive(ctx);
            gracefulClose(ctx);
            //will issue the interrupted future cancellation
            if (future != null)
                future.cancel(true);

            logger.debug(" received a close : num live: " + liveConns.size() + " num pops: " + numActiveConnPops +
                    " num pushes: " + numActiveConnPushes + " : " + ctx.channel().id().toString());

        }
    }

    /**
     * Gracefully writes out to the wire and decrements all the counters and channel group data
     *
     * @param ctx
     */
    public void gracefulClose(ChannelHandlerContext ctx) {
        ctx.flush();
        if (clientOp != null) {
            if (clientOp == OperationType.PUSH)
                numActiveConnPushes.decrementAndGet();
            else
                numActiveConnPops.decrementAndGet();
        }
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        // Close the connection when an exception is raised.
        gracefulClose(ctx);
        logger.error("Netty exception >>> " + cause.getLocalizedMessage());
        ctx.close();
    }
}
