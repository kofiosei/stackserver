package com.solublesoft.stackserver;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * Created by oseik on 12/14/16.
 */
public class DebugHandler extends SimpleChannelInboundHandler<ByteBuf> {

    @Override
    public void channelRead0(ChannelHandlerContext ctx, ByteBuf buf) throws Exception {
        PhiloLifo pl =  PhiloLifo.SINGLETON;
        byte[] strBytes = new byte[buf.readableBytes()];
        buf.readBytes(strBytes);
        String input = new String(strBytes);
        input = input.trim();
        if (input.toLowerCase().equals("reset")) {
            pl.resetStack();
            ctx.writeAndFlush("Stack reset\n".getBytes());
        }
        else if (input.toLowerCase().equals("debug")) {
            StringBuilder currStack = new StringBuilder();
            currStack.append(pl.toString())
            .append("\nLive connections: "+PushPopHandler.liveConns.toString())
            .append("\n");
            ctx.writeAndFlush(currStack.toString().getBytes());
        }
        else{
            ctx.writeAndFlush("Allowed operations are \"debug\" or \"reset\"".getBytes());
        }
        //ctx.close();
    }
}
