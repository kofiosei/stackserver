package com.solublesoft.stackserver;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by oseik on 12/10/16.
 */
public class PushPopDecoder extends ByteToMessageDecoder {
    Logger logger = LoggerFactory.getLogger(TimeBasedChannelGroup.class);
    private int cumulativeSize = 0;
    private final byte opMask = (byte) 0x80;
    private final byte lengthMask = (byte) 0x7F;

    // On every message ... if there are some bytes to be read , accumulate
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        // get the first byte of the input .
        // get bit wise masks to see what operation we should perform ... and also get the length of the data if
        // applicable from the rest of the header
        // WARNING ... can't use any method that modifies the reader or writer index otherwise decode will not keep
        // calling itself to wait for more data
        byte header = in.getByte(0);
        PushPopHandler.OperationType clientOp = ((header & opMask) == 0) ? PushPopHandler.OperationType.PUSH : PushPopHandler.OperationType.POP;
        cumulativeSize = header & lengthMask;
        logger.debug("channel: " + ctx.channel().id() + " buf size: " + in.readableBytes() + " , final size: " + cumulativeSize);

        if (clientOp == PushPopHandler.OperationType.POP) {
            // modify the reader index to advance past the first byte
            in.readByte();
            logger.debug("DECODER: doing a pop with channel " + ctx.channel().id());
            out.add(new StackOperationDirective(ctx.channel(), clientOp, new byte[1]));
        }

        //if we are doing pushes ... wait for all the data first. Useful for the slow updates
        if ((in.readableBytes() < cumulativeSize + 1) && (clientOp != PushPopHandler.OperationType.POP)) {
            return;
        }

        //when we finally have enough ... build the stack operation object

        if (clientOp == PushPopHandler.OperationType.PUSH) {
            in.readByte(); //move the reader index so that we are only dealing with the data itself
            byte[] data = new byte[cumulativeSize];
            in.readBytes(data);
            out.add(new StackOperationDirective(ctx.channel(), clientOp, data));
        }
    }
}