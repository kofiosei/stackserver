package com.solublesoft.stackserver;

import io.netty.buffer.ByteBuf;

/**
 * Created by oseik on 12/10/16.
 */
public class LifoEntry {

    private byte[] someBytes ;
    private long stackInsertTime = 0L;

    public LifoEntry(ByteBuf buf) {
        someBytes = new byte[buf.readableBytes()];
        buf.readBytes(someBytes);
    }

    public LifoEntry(byte[] bufBytes) {
        someBytes = bufBytes;
    }

    public byte[] getData() {
        return someBytes;
    }

    //sets the stack insert time of the entry into the stack
    public void setTime(){
        stackInsertTime = System.currentTimeMillis();
    }

    //returns the time the stack item was inserted
    public long getTime(){
        return stackInsertTime;
    }

    @Override
    public String toString() {
        return "{\"value\": "+new String(someBytes)+" \"time\": "+String.valueOf(this.getTime())+"}";
    }

}
