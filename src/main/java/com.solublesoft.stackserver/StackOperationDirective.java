package com.solublesoft.stackserver;

import io.netty.channel.Channel;

/**
 * Created by oseik on 12/15/16.
 */
public class StackOperationDirective {
    private PushPopHandler.OperationType  operation;
    private byte[] stackData;
    private Channel channel;

    //The object that has all the information the PushPopHandler needs ... so that we are not dealing
    // with netty bytebuf
    public StackOperationDirective(Channel channel, PushPopHandler.OperationType opType, byte[] stackData) {
        this.operation = opType;
        this.channel = channel;
        this.stackData = stackData;
    }

    public Channel getChannel() {
        return channel;
    }

    public PushPopHandler.OperationType getOperation() {
        return operation;
    }

    public byte[] getData() {
        return stackData;
    }
}
